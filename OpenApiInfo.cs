﻿using System;
using Swashbuckle.AspNetCore.Swagger;

namespace hubNotificacionesTrabajo.API
{
    internal class OpenApiInfo : Info
    {
        public string Version { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Uri TermsOfService { get; set; }
        public object Contact { get; set; }
        public object License { get; set; }
    }
}