﻿using hubNotificacionesTrabajo.API.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Domain.Security
{
    public class InMemoryGetAllApiKeysQuery : IGetAllApiKeysQuery
    {
        public Task<IReadOnlyDictionary<string, ApiKey>> ExecuteAsync()
        {
            var apiKeys = new List<ApiKey>
        {
            new ApiKey(1, "App-Trabajo", "1e541260258862388c766ffc65ded0774c846b6ea053d91704c30085398c8351", new DateTime(2019, 01, 01),
                    new List<string>
                    {
                        "",
                    })
        };

            IReadOnlyDictionary<string, ApiKey> readonlyDictionary = apiKeys.ToDictionary(x => x.Key, x => x);
            return Task.FromResult(readonlyDictionary);
        }
    }

}
