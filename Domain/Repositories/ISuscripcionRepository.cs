﻿using hubNotificacionesTrabajo.API.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Domain.Repositories
{
    public interface ISuscripcionRepository
    {
        Suscripcion FindByIdAsync(long idUsuarioTrabajo, long idTag);
        void Remove(Suscripcion suscripcion);
        bool Find(long idTag, long idUsuario);
    }
}
