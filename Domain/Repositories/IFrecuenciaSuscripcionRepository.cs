﻿using hubNotificacionesTrabajo.API.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Domain.Repositories
{
    public interface IFrecuenciaSuscripcionRepository
    {
        Task AddFrecuenciaSuscripcionAsync(FrecuenciaSuscripcion frecuenciaSuscripcion);
        void Remove(FrecuenciaSuscripcion frecuenciaSuscripcion);
        bool Find(long idFrecuencia, long idUsuario);
    }
}
