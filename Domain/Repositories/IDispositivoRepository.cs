﻿using hubNotificacionesTrabajo.API.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Domain.Repositories
{
    public interface IDispositivoRepository
    {
        bool Find(IList<Dispositivo> dispositivos);
        Task<IList<Dispositivo>> ListDispositivoAsync(long id);
        Task AddDispositivoAsync(Dispositivo dispositivo);

        Dispositivo FindByIdAsync(long idUsuarioTrabajo, string idDispositivo);
        void Remove(Dispositivo dispositivo);
        //bool Find(long idDispositivo, long idUsuarioTrabajo);
    }
}
