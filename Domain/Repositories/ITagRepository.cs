﻿using hubNotificacionesTrabajo.API.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Domain.Repositories
{
    public interface ITagRepository
    {
        Task<IEnumerable<Tag>> ListAsync();
        Task <Tag> FindByIdAsync(long id);
        Tag FindByDescAsync(string descripcion);
        Task AddAsync(Tag tag);
        void UpdateAsync(Tag tag);
        void Remove(Tag tag);
        bool Find(IList<Suscripcion> suscripcions);
    }
}
