﻿using System.Collections.Generic;
using System.Threading.Tasks;
using hubNotificacionesTrabajo.API.Domain.Models;
using hubNotificacionesTrabajo.API.Domain.Services.Communication;

namespace hubNotificacionesTrabajo.API.Domain.Repositories
{
    public interface IUsuarioRepository
    {
        Task<IEnumerable<Usuario>> ListAsync();
        Task<IList<Suscripcion>> ListSuscripcionAsync(long id);
        Task AddAsync(Usuario usuario);
        bool Find(string email, long idusuariotrabajo);
        Task<Usuario> FindByIdAsync(long id);
        bool ExistIdUsuarioGallito(long id);
        bool ExistEmailUsuario(string email);
        void Remove(Usuario usuario);
        void Update(Usuario usuario);
        Task AddSuscripcionAsync(Suscripcion suscripcion);

        Task<IList<FrecuenciaSuscripcion>> ListFrecuenciaSuscripcionAsync(long id);
    }
}