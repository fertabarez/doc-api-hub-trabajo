﻿using hubNotificacionesTrabajo.API.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Domain.Repositories
{
    public interface IFrecuenciaRepository
    {
        Task<IEnumerable<Frecuencia>> ListAsync();
        Task<Frecuencia> FindByIdAsync(long id);
    }
}
