﻿using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Domain.Repositories
{
    public interface IUnitOfWork
    {
        Task CompleteAsync();
    }
}
