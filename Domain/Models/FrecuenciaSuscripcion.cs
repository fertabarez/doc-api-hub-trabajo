﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Domain.Models
{
    public class FrecuenciaSuscripcion
    {
        public long Id { get; set; }
        public long IdUsuarioTrabajo { get; set; }        
        public long IdFrecuencia { get; set; }
        public DateTime Fecha { get; set; } = DateTime.Now;
        public Usuario Usuario { get; set; }
        public Frecuencia Frecuencia { get; set; }
    }
}
