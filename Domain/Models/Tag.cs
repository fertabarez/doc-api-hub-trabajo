﻿using System.Collections.Generic;
using hubNotificacionesTrabajo.API.Domain.Models;
using System;

namespace hubNotificacionesTrabajo.API.Domain.Models
{
    public class Tag
    {
        public long IdTag { get; set; }
        public string Descripcion { get; set; }        
        public DateTime Fecha { get; set; } = DateTime.Now;
        public IList<Suscripcion> Suscripcion { get; set; } = new List<Suscripcion>();
    }
}