﻿using System.Collections.Generic;
using hubNotificacionesTrabajo.API.Domain.Models;
using System;

namespace hubNotificacionesTrabajo.API.Domain.Models
{
    public class Suscripcion
    {
        public long Id { get; set; }
        public long IdUsuarioTrabajo { get; set; }        
        public long IdTag { get; set; }        
        public DateTime Fecha { get; set; } = DateTime.Now;
        public Usuario Usuario { get; set; }
        public Tag Tag { get; set; }
    }
}