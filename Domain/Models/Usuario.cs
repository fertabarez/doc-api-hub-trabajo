﻿using System.Collections.Generic;
using hubNotificacionesTrabajo.API.Domain.Models;
using System;

namespace hubNotificacionesTrabajo.API.Domain.Models
{
    public class Usuario
    {        
        public long IdUsuarioTrabajo { get; set; }
        public long IdUsuarioGallito { get; set; }        
        public string Email { get; set; }        
        public DateTime Fecha { get; set; } = DateTime.Now; 
        public IList<Dispositivo> Dispositivos { get; set; } = new List<Dispositivo>();
        public IList<Suscripcion> Suscripciones { get; set; } = new List<Suscripcion>();
        public IList<FrecuenciaSuscripcion> Frecuencias { get; set; } = new List<FrecuenciaSuscripcion>();
    }
}