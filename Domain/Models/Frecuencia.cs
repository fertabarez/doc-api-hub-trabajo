﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Domain.Models
{
    public class Frecuencia
    {
        public long IdFrecuencia { get; set; }
        public string Descripcion { get; set; }
        public IList<FrecuenciaSuscripcion> Frecuencias { get; set; } = new List<FrecuenciaSuscripcion>();
    }
}
