﻿using System.Collections.Generic;
using hubNotificacionesTrabajo.API.Domain.Models;
using System;

namespace hubNotificacionesTrabajo.API.Domain.Models
{
    public class Dispositivo
    {
        public long IdUsuarioTrabajo { get; set; }
        public string IdDispositivo { get; set; }
        public string VendorId { get; set; }
        public DateTime Fecha { get; set; } = DateTime.Now;
        public Usuario Usuario { get; set; } = new Usuario();
    }
}

