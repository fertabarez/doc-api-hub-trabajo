﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Domain.Exceptions.DispositivoException
{
    public class DispositivoYaRegistradoException: DispositivoException
    {
        public DispositivoYaRegistradoException() : base("Dispositivo ya registrado en el sistema")
        {
        }
        public DispositivoYaRegistradoException(string message) : base(message)
        {
        }
    }
}
