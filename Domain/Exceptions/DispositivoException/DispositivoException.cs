﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Domain.Exceptions.DispositivoException
{
    public class DispositivoException : Exception
    {
        public DispositivoException(string message) : base(message)
        {
        }
    }
}
