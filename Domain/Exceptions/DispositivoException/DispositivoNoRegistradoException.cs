﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Domain.Exceptions.DispositivoException
{
    public class DispositivoNoRegistradoException : DispositivoException
    {
        public DispositivoNoRegistradoException() : base("Dispositivo no registrado en el sistema")
        {
        }
        public DispositivoNoRegistradoException(string message) : base(message)
        {
        }
    }
}
