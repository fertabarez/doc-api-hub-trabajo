﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Domain.Exceptions.FrecuenciaException
{
    public class FrecuenciaException: Exception
    {
        public FrecuenciaException(string message) : base(message)
        {
        }
    }
}
