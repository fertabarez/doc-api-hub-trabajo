﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Domain.Exceptions.UsuarioException
{
    public class UsuarioNoRegistradoException : UsuarioException
    {
        public UsuarioNoRegistradoException() : base("Usuario no registrado en el sistema.")
        {
        }
        public UsuarioNoRegistradoException(string message) : base(message)
        {
        }
    
    }
}
