﻿using System;

namespace hubNotificacionesTrabajo.API.Domain.Exceptions.UsuarioException
{
    public class UsuarioException : Exception
    {
        public UsuarioException(string message) : base(message)
        {
        }
    }
}