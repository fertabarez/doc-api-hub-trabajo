﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Domain.Exceptions.UsuarioException
{
    public class UsuarioRegistradoException : UsuarioException
    {
        public UsuarioRegistradoException() : base("Usuario ya registrado en el sistema.")
        {
        }
        public UsuarioRegistradoException(string message) : base(message)
        {
        }
    
    }
}
