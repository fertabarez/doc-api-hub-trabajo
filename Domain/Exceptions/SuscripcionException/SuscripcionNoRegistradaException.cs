﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Domain.Exceptions.SuscripcionException
{
    public class SuscripcionNoRegistradaException: SuscripcionException
    {
        public SuscripcionNoRegistradaException() : base("Suscripcion no registrada en el sistema.")
        {
        }
        public SuscripcionNoRegistradaException(string message) : base(message)
        {
        }
    }
}
