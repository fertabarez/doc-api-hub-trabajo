﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Domain.Exceptions.SuscripcionException
{
    public class SuscripcionRegistradaException: SuscripcionException
    {
        public SuscripcionRegistradaException() : base("Suscripcion ya registrada en el sistema.")
        {
        }
        public SuscripcionRegistradaException(string message) : base(message)
        {
        }
    }
}
