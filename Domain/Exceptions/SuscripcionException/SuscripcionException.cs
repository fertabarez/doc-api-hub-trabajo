﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Domain.Exceptions.SuscripcionException
{
    public class SuscripcionException : Exception {
        public SuscripcionException(string message) : base(message)
        {
        }
    }
}
