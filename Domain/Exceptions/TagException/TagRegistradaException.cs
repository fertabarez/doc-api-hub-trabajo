﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Domain.Exceptions.TagException
{
    public class TagRegistradaException : TagException
    {
        public TagRegistradaException() : base("Tag ya registrada en el sistema")
        {
        }
        public TagRegistradaException(string message) : base(message)
        {
        }
    }
}


