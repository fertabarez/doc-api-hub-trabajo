﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Domain.Exceptions.TagException
{
    public class TagNoRegistradaException: TagException
    {
        public TagNoRegistradaException() : base("Tag no registrada en el sistema")
        {
        }
        public TagNoRegistradaException(string message) : base(message)
        {
        }
    }
}
