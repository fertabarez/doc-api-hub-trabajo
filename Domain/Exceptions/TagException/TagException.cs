﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Domain.Exceptions.TagException
{
    public class TagException : Exception
    {
        public TagException(string message) : base(message)
        {
        }
    }
}
