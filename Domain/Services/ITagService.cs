﻿using hubNotificacionesTrabajo.API.Domain.Models;
using hubNotificacionesTrabajo.API.Domain.Services.Communication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Domain.Services
{
    public interface ITagService
    {
        Task<IEnumerable<Tag>> ListAsync();
        Task<TagResponse> SaveAsync(Tag tag);
        Task<TagResponse> UpdateAsync(long id, Tag tag);
        Task<TagResponse> DeleteTagAsync(long id);
    }
}
