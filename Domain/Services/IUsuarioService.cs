﻿using System.Collections.Generic;
using System.Threading.Tasks;
using hubNotificacionesTrabajo.API.Domain.Models;
using hubNotificacionesTrabajo.API.Domain.Services.Communication;
using hubNotificacionesTrabajo.API.Resources;

namespace hubNotificacionesTrabajo.API.Domain.Services
{
    public interface IUsuarioService
    {
        Task<IEnumerable<Usuario>> ListUsuarioAsync();
        Task<UsuarioResponse> UpdateUsuarioAsync(long id, Usuario usuario);
        Task<UsuarioResponse> SaveUsuarioAsync(Usuario usuario);
        Task<SaveUsuarioResponse> GetUsuarioById(long id);
        Task<UsuarioResponse> DeleteUsuarioAsync(long id);
        Task<SuscripcionResponse> DeleteSuscripcionAsync(long id, long idSuscripcion);
        Task<SuscripcionResponse> DeleteAllSuscripcionAsync(long id);        
        Task<FrecuenciaSuscripcionResponse> DeleteAllFrecuenciasAsync(long id);
        Task<DispositivoResponse> DeleteDispositivoAsync(long id, string idDispositivo);
        Task<SuscripcionResponse> SaveSuscripcionAsync(Suscripcion suscripcion);
        Task<FrecuenciaSuscripcionResponse> SaveFrecuenciaSuscripcionAsync(FrecuenciaSuscripcion frecuenciaSuscripcion);
        Task<DispositivoResponse> SaveDispositivoAsync(Dispositivo dispositivo);
        Task<IEnumerable<Suscripcion>> ListSuscripcionAsync(long id); 
        Task<IEnumerable<Dispositivo>> ListDspositivoAsync(long id); 
    }
}