﻿using hubNotificacionesTrabajo.API.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Domain.Services.Communication
{
    public class DispositivoResponse: BaseResponse
    {
        public Dispositivo Dispositivo { get; private set; }

        public DispositivoResponse(bool success, string message, Dispositivo dispositivo) : base(success, message)
        {
            Dispositivo = dispositivo;
        }

        /// <summary>
        /// Creates a success response.
        /// </summary>
        /// <param name="usuario">Saved category.</param>
        /// <returns>Response.</returns>
        public DispositivoResponse(Dispositivo dispositivo) : this(true, string.Empty, dispositivo)
        { }

        /// <summary>
        /// Creates am error response.
        /// </summary>
        /// <param name="message">Error message.</param>
        /// <returns>Response.</returns>
        public DispositivoResponse(string message) : this(false, message, null)
        { }
    }
}
