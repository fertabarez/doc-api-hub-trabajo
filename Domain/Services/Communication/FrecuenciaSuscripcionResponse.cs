﻿using hubNotificacionesTrabajo.API.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Domain.Services.Communication
{
    public class FrecuenciaSuscripcionResponse : BaseResponse
    {
        public FrecuenciaSuscripcion FrecuenciaSuscripcion { get; private set; }

        public FrecuenciaSuscripcionResponse(bool success, string message, FrecuenciaSuscripcion frecuenciaSuscripcion) : base(success, message)
        {
            FrecuenciaSuscripcion = frecuenciaSuscripcion;
        }

        /// <summary>
        /// Creates a success response.
        /// </summary>
        /// <param name="usuario">Saved category.</param>
        /// <returns>Response.</returns>
        public FrecuenciaSuscripcionResponse(FrecuenciaSuscripcion frecuenciaSuscripcion) : this(true, string.Empty, frecuenciaSuscripcion)
        { }

        /// <summary>
        /// Creates am error response.
        /// </summary>
        /// <param name="message">Error message.</param>
        /// <returns>Response.</returns>
        public FrecuenciaSuscripcionResponse(string message) : this(false, message, null)
        { }
    }
}
