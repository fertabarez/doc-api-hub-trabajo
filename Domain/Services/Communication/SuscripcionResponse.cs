﻿using hubNotificacionesTrabajo.API.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Domain.Services.Communication
{
    public class SuscripcionResponse : BaseResponse
    {
        public Suscripcion Suscripcion { get; private set; }        

        public SuscripcionResponse(bool success, string message, Suscripcion suscripcion) : base(success, message)
        {
            Suscripcion = suscripcion;
        }

        /// <summary>
        /// Creates a success response.
        /// </summary>
        /// <param name="usuario">Saved category.</param>
        /// <returns>Response.</returns>
        public SuscripcionResponse(Suscripcion suscripcion) : this(true, string.Empty, suscripcion)
        { }

        /// <summary>
        /// Creates am error response.
        /// </summary>
        /// <param name="message">Error message.</param>
        /// <returns>Response.</returns>
        public SuscripcionResponse(string message) : this(false, message, null)
        { }
    }
}
