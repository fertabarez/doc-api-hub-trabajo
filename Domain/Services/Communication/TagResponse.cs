﻿using hubNotificacionesTrabajo.API.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Domain.Services.Communication
{
    public class TagResponse : BaseResponse
    {
        public Tag Tag { get; private set; }

        public TagResponse(bool success, string message, Tag tag) : base(success, message)
        {
            Tag =  tag;
        }

        /// <summary>
        /// Creates a success response.
        /// </summary>
        /// <param name="usuario">Saved category.</param>
        /// <returns>Response.</returns>
        public TagResponse(Tag tag) : this(true, string.Empty, tag)
        { }

        /// <summary>
        /// Creates am error response.
        /// </summary>
        /// <param name="message">Error message.</param>
        /// <returns>Response.</returns>
        public TagResponse(string message) : this(false, message, null)
        { }
    }
    
}
