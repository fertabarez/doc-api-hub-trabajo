﻿using hubNotificacionesTrabajo.API.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Domain.Services.Communication
{
    public class SaveUsuarioResponse : BaseResponse
    {
        public SaveUsuarioResource Usuario { get; private set; }

        public SaveUsuarioResponse(bool success, string message, SaveUsuarioResource usuario) : base(success, message)
        {
            Usuario = usuario;
        }

        /// <summary>
        /// Creates a success response.
        /// </summary>
        /// <param name="usuario">Saved category.</param>
        /// <returns>Response.</returns>
        public SaveUsuarioResponse(SaveUsuarioResource usuario) : this(true, string.Empty, usuario)
        { }

        /// <summary>
        /// Creates am error response.
        /// </summary>
        /// <param name="message">Error message.</param>
        /// <returns>Response.</returns>
        public SaveUsuarioResponse(string message) : this(false, message, null)
        { }
    
    }
}
