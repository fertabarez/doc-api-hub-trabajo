﻿using hubNotificacionesTrabajo.API.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Domain.Services
{
    public interface IFrecuenciaService
    {
        Task<IEnumerable<Frecuencia>> ListAsync();
    }
}
