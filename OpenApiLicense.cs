﻿using System;

namespace hubNotificacionesTrabajo.API
{
    internal class OpenApiLicense
    {
        public string Name { get; set; }
        public Uri Url { get; set; }
    }
}