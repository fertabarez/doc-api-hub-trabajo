﻿using System;

namespace hubNotificacionesTrabajo.API
{
    internal class OpenApiContact
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public Uri Url { get; set; }
    }
}