﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using hubNotificacionesTrabajo.API.Domain.Models;
using hubNotificacionesTrabajo.API.Domain.Services;

using hubNotificacionesTrabajo.API.Resources;
using hubNotificacionesTrabajo.API.Extensions;
using AutoMapper;
using System;
using hubNotificacionesTrabajo.API.Domain.Exceptions.UsuarioException;
using hubNotificacionesTrabajo.API.Domain.Exceptions.DispositivoException;
using hubNotificacionesTrabajo.API.Domain.Exceptions.SuscripcionException;
using hubNotificacionesTrabajo.API.Domain.Exceptions.TagException;
using Microsoft.AspNetCore.Authorization;
using hubNotificacionesTrabajo.API.Domain.Exceptions.FrecuenciaException;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Diagnostics;
using System.Net;

namespace hubNotificacionesTrabajo.API.Controllers
{
    [Route("/api/v1/[controller]")]
    [Authorize]
    public class UsuarioController : Controller
    {
        private readonly IUsuarioService _usuarioService;
        private readonly IMapper _mapper;

        public UsuarioController(IUsuarioService usuarioService, IMapper mapper)
        {
            _usuarioService = usuarioService;
            _mapper = mapper;
        }

        ///// <summary>
        ///// Obtener Listado de Usuario.
        ///// </summary>        
        ///// <param name="item"></param>
        ///// <returns>A newly created TodoItem</returns>
        ///// <response code="200">Listado de tags</response>
        ///// <response code="500">        
        /////{
        /////  "errors": [
        /////    {
        /////      "status": "500",
        /////      "detail": "Ha ocurrido un error inesperado."
        /////    }
        /////  ]
        /////}
        ///// </response>
        //[HttpGet]
        //[Authorize]
        //public async Task<IEnumerable<UsuarioResource>> GetAllAsync()
        //{
        //    var usuario = await _usuarioService.ListAsync();
        //    var resources = _mapper.Map<IEnumerable<Usuario>, IEnumerable<UsuarioResource>>(usuario);
        //    return resources;
        //}

        /// <summary>
        /// Obtener Usuario
        /// </summary>
        /// <param name="resource"></param>
        /// <returns></returns>
        /// <response code="200">Usuario</response> 
        /// <response code="401">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "401",
        ///      "detail": "Sin autorizacion."
        ///    }
        ///  ]
        ///}
        /// </response>
        ///<response code="404">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "404",
        ///      "detail": "Usuario no registrado en el sistema."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// <response code="500">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "500",
        ///      "detail": "Ha ocurrido un error inesperado."
        ///    }
        ///  ]
        ///}
        /// </response>
        [HttpGet("{id}")]
        [Authorize]
        [ProducesResponseType(typeof(SaveUsuarioResource), 200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> GetUserByIdAsync(long id)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState.GetErrorMessages());

                var result = await _usuarioService.GetUsuarioById(id);
                if (!result.Success)
                    return BadRequest(result.Message);

                return Ok(result.Usuario);
            }
            catch (UsuarioNoRegistradoException unre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("404", unre.Message);
                er.Errors.Add(e);
                return NotFound(er);
            }
            catch (Exception ex)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("500", "Ha ocurrido un error inesperado.");
                er.Errors.Add(e);
                return StatusCode(500, er);
            }
        }

        /// <summary>
        /// Registrar Usuario
        /// </summary>
        /// <param name="resource"></param>
        /// <returns></returns>
        /// <response code="200">Usuario registrado</response>        
        ///<response code="400">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "400",
        ///      "detail": "Usuario ya registrado en el sistema."
        ///    }
        ///  ]
        ///}
        /// <response code="401">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "401",
        ///      "detail": "Sin autorizacion."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// </response>
        /// /// <response code="404">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "404",
        ///      "detail": "Tag no registrada en el sistema."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// <response code="500">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "500",
        ///      "detail": "Ha ocurrido un error inesperado."
        ///    }
        ///  ]
        ///}
        /// </response>
        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(SaveUsuarioResource), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> PostUsuarioAsync([FromBody] SaveUsuarioResource resource)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState.GetErrorMessages());

                var usuario = _mapper.Map<SaveUsuarioResource, Usuario>(resource);
                var result = await _usuarioService.SaveUsuarioAsync(usuario);

                if (!result.Success)
                    return BadRequest(result.Message);

                var usuarioResource = _mapper.Map<Usuario, UsuarioResource>(result.Usuario);
                return Ok(usuarioResource);
            }
            catch (UsuarioRegistradoException ure)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("400", ure.Message);
                er.Errors.Add(e);
                return BadRequest(er);
            }
            catch (DispositivoYaRegistradoException dre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("400", dre.Message);
                er.Errors.Add(e);
                return BadRequest(er);
            }
            catch (TagNoRegistradaException tnre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("404", tnre.Message);
                er.Errors.Add(e);
                return NotFound(er);
            }
            catch (Exception ex)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("500", "Ha ocurrido un error inesperado.");
                er.Errors.Add(e);
                return StatusCode(500, er);
            }
        }
        /// <summary>
        /// Modificar Usuario
        /// </summary>
        /// <param name="resource"></param>
        /// <returns></returns>
        /// <response code="200">Usuario modificado</response>        
        ///<response code="400">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "400",
        ///      "detail": "Ya existe un usuario registrado con ese email."
        ///    }
        ///    {
        ///      "status": "400",
        ///      "detail": "El email de usuario no puede estar vacio."
        ///    }
        ///    {
        ///      "status": "400",
        ///      "detail": "Ya existe un usuario registrado en con ese id de usuario gallito en el sistema."
        ///    }
        ///  ]
        ///}
        ///
        /// </response>
        /// <response code="401">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "401",
        ///      "detail": "Sin autorizacion."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// <response code="404">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "404",
        ///      "detail": "Usuario no registrada en el sistema."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// <response code="500">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "500",
        ///      "detail": "Ha ocurrido un error inesperado."
        ///    }
        ///  ]
        ///}
        /// </response>
        [HttpPut("{id}")]
        [Authorize]
        [ProducesResponseType(typeof(SaveUsuarioResource), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> PutUsuarioAsync(long id, [FromBody] UpdateUsuarioResource resource)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState.GetErrorMessages());

                var usuario = _mapper.Map<UpdateUsuarioResource, Usuario>(resource);
                var result = await _usuarioService.UpdateUsuarioAsync(id, usuario);
                if (!result.Success)
                    return BadRequest(result.Message);

                var usuarioResource = _mapper.Map<Usuario, UsuarioResource>(result.Usuario);
                return Ok(usuarioResource);
            }
            catch (UsuarioRegistradoException unre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("400", unre.Message);
                er.Errors.Add(e);
                return BadRequest(er);
            }
            catch (UsuarioNoRegistradoException unre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("404", unre.Message);
                er.Errors.Add(e);
                return NotFound(er);
            }
            catch (UsuarioException unre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("400", unre.Message);
                er.Errors.Add(e);
                return BadRequest(er);
            }
            catch (Exception ex)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("500", "Ha ocurrido un error inesperado.");
                er.Errors.Add(e);
                return StatusCode(500, er);
            }
        }

        /// <summary>
        /// Eliminar Usuario
        /// </summary>
        /// <param name="resource"></param>
        /// <returns></returns>
        /// <response code="200">Usuario eliminado</response>  
        /// <response code="401">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "401",
        ///      "detail": "Sin autorizacion."
        ///    }
        ///  ]
        ///}
        /// </response>
        ///<response code="404">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "404",
        ///      "detail": "Usuario no registrada en el sistema."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// <response code="500">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "500",
        ///      "detail": "Ha ocurrido un error inesperado."
        ///    }
        ///  ]
        ///}
        /// </response>
        [HttpDelete("{id}")]
        [Authorize]
        [ProducesResponseType(typeof(SaveUsuarioResource), 200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> DeleteUsuarioAsync(long id)
        {
            try
            {
                var result = await _usuarioService.DeleteUsuarioAsync(id);

                if (!result.Success)
                    return BadRequest(result.Message);

                var usuarioResource = _mapper.Map<Usuario, UsuarioResource>(result.Usuario);
                return Ok(usuarioResource);
            }
            catch (UsuarioNoRegistradoException unre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("404", unre.Message);
                er.Errors.Add(e);
                return NotFound(er);
            }
            catch (Exception ex)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("500", "Ha ocurrido un error inesperado.");
                er.Errors.Add(e);
                return StatusCode(500, er);
            }
        }

        /// <summary>
        /// Obtener suscripciones de Usuario
        /// </summary>
        /// <param name="resource"></param>
        /// <returns></returns>
        /// <response code="200">Listado de suscripciones</response>   
        /// <response code="401">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "401",
        ///      "detail": "Sin autorizacion."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// <response code="500">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "500",
        ///      "detail": "Ha ocurrido un error inesperado."
        ///    }
        ///  ]
        ///}
        /// </response>
        [HttpGet("{id}/suscripcion")]
        [Authorize]
        [ProducesResponseType(typeof(IEnumerable<SuscripcionResource>), 200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> ListSuscripcionAsync(long id)
        {
            try
            {
                var result = await _usuarioService.ListSuscripcionAsync(id);
                var resources = _mapper.Map<IEnumerable<Suscripcion>, IEnumerable<SuscripcionResource>>(result);
                return Ok(resources);
            }
            catch (UsuarioNoRegistradoException unre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("404", unre.Message);
                er.Errors.Add(e);
                return NotFound(er);
            }
            catch (Exception ex)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("500", "Ha ocurrido un error inesperado.");
                er.Errors.Add(e);
                return StatusCode(500, er);
            }

        }

        /// <summary>
        /// Registro suscripcion
        /// </summary>
        /// <param name="resource"></param>
        /// <returns></returns>
        /// <response code="200">Usuario modificado</response>        
        ///<response code="400">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "400",
        ///      "detail": "Suscripcion ya registrada para el usuario."
        ///    }
        ///  ]
        ///}        
        /// </response>
        /// <response code="401">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "401",
        ///      "detail": "Sin autorizacion."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// <response code="404">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "404",
        ///      "detail": "Usuario no registrado en el sistema."
        ///    }
        ///    {
        ///      "status": "404",
        ///      "detail": "Tag no registrada en el sistema."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// <response code="500">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "500",
        ///      "detail": "Ha ocurrido un error inesperado."
        ///    }
        ///  ]
        ///}
        /// </response>
        [HttpPost("{id}/suscripcion")]
        [Authorize]
        [ProducesResponseType(typeof(IEnumerable<SuscripcionResource>), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> PostSuscripcionAsync(long id, [FromBody] SaveSuscripcionResource resource)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState.GetErrorMessages());


                foreach (SuscripcionResource sr in resource.Suscripciones)
                {
                    var s = _mapper.Map<SuscripcionResource, Suscripcion>(sr);
                    s.IdUsuarioTrabajo = id;
                    s.Fecha = DateTime.Now;
                    await _usuarioService.SaveSuscripcionAsync(s);
                }
                return Ok(resource);
            }
            catch (UsuarioNoRegistradoException unre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("404", unre.Message);
                er.Errors.Add(e);
                return NotFound(er);
            }
            catch (TagNoRegistradaException tnre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("404", tnre.Message);
                er.Errors.Add(e);
                return NotFound(er);
            }
            catch (SuscripcionRegistradaException unre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("400", unre.Message);
                er.Errors.Add(e);
                return BadRequest(er);
            }
            catch (Exception ex)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("500", "Ha ocurrido un error inesperado.");
                er.Errors.Add(e);
                return StatusCode(500, er);
            }
        }
        /// <summary>
        /// Actualizacion suscripcion
        /// </summary>
        /// <param name="resource"></param>
        /// <returns></returns>
        /// <response code="200">Usuario modificado</response>                
        /// <response code="401">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "401",
        ///      "detail": "Sin autorizacion."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// <response code="404">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "404",
        ///      "detail": "Usuario no registrado en el sistema."
        ///    }
        ///    {
        ///      "status": "404",
        ///      "detail": "Tag no registrada en el sistema."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// <response code="500">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "500",
        ///      "detail": "Ha ocurrido un error inesperado."
        ///    }
        ///  ]
        ///}
        /// </response>
        [HttpPut("{id}/suscripcion")]
        [Authorize]
        [ProducesResponseType(typeof(IEnumerable<SuscripcionResource>), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> PutSuscripcionAsync(long id, [FromBody] SaveSuscripcionResource resource)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState.GetErrorMessages());

                var result = await _usuarioService.DeleteAllSuscripcionAsync(id);

                foreach (SuscripcionResource sr in resource.Suscripciones)
                {
                    var s = _mapper.Map<SuscripcionResource, Suscripcion>(sr);
                    s.IdUsuarioTrabajo = id;
                    s.Fecha = DateTime.Now;
                    await _usuarioService.SaveSuscripcionAsync(s);
                }
                return Ok(resource);
            }
            catch (UsuarioNoRegistradoException unre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("404", unre.Message);
                er.Errors.Add(e);
                return NotFound(er);
            }
            catch (TagNoRegistradaException tnre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("404", tnre.Message);
                er.Errors.Add(e);
                return NotFound(er);
            }
            catch (SuscripcionRegistradaException unre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("400", unre.Message);
                er.Errors.Add(e);
                return BadRequest(er);
            }
            catch (Exception ex)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("500", "Ha ocurrido un error inesperado.");
                er.Errors.Add(e);
                return StatusCode(500, er);
            }
        }

        /// <summary>
        /// Actualizacion suscripcion y frecuencias
        /// </summary>
        /// <param name="resource"></param>
        /// <returns></returns>
        /// <response code="200">Suscripcion y frecuencias modificadas</response>                
        /// <response code="401">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "401",
        ///      "detail": "Sin autorizacion."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// <response code="404">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "404",
        ///      "detail": "Usuario no registrado en el sistema."
        ///    },
        ///    {
        ///      "status": "404",
        ///      "detail": "Tag no registrada en el sistema."
        ///    },
        ///    {
        ///      "status": "404",
        ///      "detail": "Frecuencia no registrada en el sistema."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// <response code="500">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "500",
        ///      "detail": "Ha ocurrido un error inesperado."
        ///    }
        ///  ]
        ///}
        /// </response>
        [HttpPut("{id}/suscripcionfrecuencia")]
        [Authorize]
        [ProducesResponseType(typeof(IEnumerable<UpdateSuscripcionFrecuenciaResource>), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> PutSuscripcionFrecuenciaAsync(long id, [FromBody] UpdateSuscripcionFrecuenciaResource resource)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState.GetErrorMessages());

                var result = await _usuarioService.DeleteAllSuscripcionAsync(id);

                foreach (TagResourceOnlyId sr in resource.Suscripciones)
                {
                    var s = _mapper.Map<TagResourceOnlyId, Suscripcion>(sr);
                    s.IdUsuarioTrabajo = id;
                    s.Fecha = DateTime.Now;
                    await _usuarioService.SaveSuscripcionAsync(s);
                }

                var result2 = await _usuarioService.DeleteAllFrecuenciasAsync(id);

                foreach (FrecuenciaSuscripcionResource sr in resource.Frecuencias)
                {
                    var s = _mapper.Map<FrecuenciaSuscripcionResource, FrecuenciaSuscripcion>(sr);
                    s.IdUsuarioTrabajo = id;
                    s.Fecha = DateTime.Now;
                    await _usuarioService.SaveFrecuenciaSuscripcionAsync(s);
                }
                return Ok(resource);
            }
            catch (UsuarioNoRegistradoException unre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("404", unre.Message);
                er.Errors.Add(e);
                return NotFound(er);
            }
            catch (TagNoRegistradaException tnre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("404", tnre.Message);
                er.Errors.Add(e);
                return NotFound(er);
            }
            catch (FrecuenciaNoRegistradaException unre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("404", unre.Message);
                er.Errors.Add(e);
                return NotFound(er);
            }
            catch (Exception ex)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("500", "Ha ocurrido un error inesperado.");
                er.Errors.Add(e);
                return StatusCode(500, er);
            }
        }

        /// <summary>
        /// Eliminar suscripcion
        /// </summary>
        /// <param name="id"></param>
        /// <param name="idsuscripcion"></param>
        /// <param name="resource"></param>
        /// <returns></returns>
        /// <response code="200">Suscripcion eliminada</response>                
        /// <response code="401">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "401",
        ///      "detail": "Sin autorizacion."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// <response code="404">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "404",
        ///      "detail": "Usuario no registrado en el sistema."
        ///    }
        ///    {
        ///      "status": "404",
        ///      "detail": "Scripcion no registrada en el sistema."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// <response code="500">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "500",
        ///      "detail": "Ha ocurrido un error inesperado."
        ///    }
        ///  ]
        ///}
        /// </response>     
        [HttpDelete("{id}/suscripcion/{idsuscripcion}")]
        [Authorize]
        [ProducesResponseType(typeof(SuscripcionResource), 200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> DeleteSuscripcionAsync(long id, long idsuscripcion)
        {
            try
            {
                var result = await _usuarioService.DeleteSuscripcionAsync(id, idsuscripcion);

                if (!result.Success)
                    return BadRequest(result.Message);

                var suscripcionResource = _mapper.Map<Suscripcion, SuscripcionResource>(result.Suscripcion);
                return Ok(suscripcionResource);
            }
            catch (UsuarioNoRegistradoException unre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("404", unre.Message);
                er.Errors.Add(e);
                return NotFound(er);
            }
            catch (SuscripcionNoRegistradaException unre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("404", unre.Message);
                er.Errors.Add(e);
                return NotFound(er);
            }
            catch (Exception ex)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("500", "Ha ocurrido un error inesperado.");
                er.Errors.Add(e);
                return StatusCode(500, er);
            }

        }

        /// <summary>
        /// Registro Frecuencia a usuario
        /// </summary>
        /// <param name="resource"></param>
        /// <returns></returns>
        /// <response code="200">Usuario modificado</response>        
        ///<response code="400">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "400",
        ///      "detail": "Frecuencia ya registrada para el usuario."
        ///    }
        ///  ]
        ///}        
        /// </response>
        /// <response code="401">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "401",
        ///      "detail": "Sin autorizacion."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// <response code="404">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "404",
        ///      "detail": "Usuario no registrado en el sistema."
        ///    }
        ///    {
        ///      "status": "404",
        ///      "detail": "Frecuencia no registrada en el sistema."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// <response code="500">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "500",
        ///      "detail": "Ha ocurrido un error inesperado."
        ///    }
        ///  ]
        ///}
        /// </response>
        [HttpPost("{id}/frecuencia")]
        [Authorize]
        [ProducesResponseType(typeof(IEnumerable<SaveFrecuenciaSuscripcionResource>), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> PostFrecuenciaSuscripcionAsync(long id, [FromBody] SaveFrecuenciaSuscripcionResource resource)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState.GetErrorMessages());


                foreach (FrecuenciaSuscripcionResource sr in resource.Frecuencias)
                {
                    var s = _mapper.Map<FrecuenciaSuscripcionResource, FrecuenciaSuscripcion>(sr);
                    s.IdUsuarioTrabajo = id;
                    s.Fecha = DateTime.Now;
                    await _usuarioService.SaveFrecuenciaSuscripcionAsync(s);
                }
                return Ok(resource);
            }
            catch (UsuarioNoRegistradoException unre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("404", unre.Message);
                er.Errors.Add(e);
                return NotFound(er);
            }
            catch (FrecuenciaNoRegistradaException fnre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("404", fnre.Message);
                er.Errors.Add(e);
                return NotFound(er);
            }
            catch (SuscripcionRegistradaException unre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("400", unre.Message);
                er.Errors.Add(e);
                return BadRequest(er);
            }
            catch (Exception ex)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("500", "Ha ocurrido un error inesperado.");
                er.Errors.Add(e);
                return StatusCode(500, er);
            }
        }

        /// <summary>
        /// Actualizacion frecuencia a usuario
        /// </summary>
        /// <param name="resource"></param>
        /// <returns></returns>
        /// <response code="200">Frecuencia modificada</response>                
        /// <response code="401">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "401",
        ///      "detail": "Sin autorizacion."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// <response code="404">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "404",
        ///      "detail": "Usuario no registrado en el sistema."
        ///    }
        ///    {
        ///      "status": "404",
        ///      "detail": "Frecuencia no registrada en el sistema."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// <response code="500">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "500",
        ///      "detail": "Ha ocurrido un error inesperado."
        ///    }
        ///  ]
        ///}
        /// </response>
        [HttpPut("{id}/frecuencia")]
        [Authorize]
        [ProducesResponseType(typeof(IEnumerable<SaveFrecuenciaResource>), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> PutFrecuenciaSuscripcionAsync(long id, [FromBody] SaveFrecuenciaSuscripcionResource resource)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState.GetErrorMessages());

                var result = await _usuarioService.DeleteAllFrecuenciasAsync(id);

                foreach (FrecuenciaSuscripcionResource sr in resource.Frecuencias)
                {
                    var s = _mapper.Map<FrecuenciaSuscripcionResource, FrecuenciaSuscripcion>(sr);
                    s.IdUsuarioTrabajo = id;
                    s.Fecha = DateTime.Now;
                    await _usuarioService.SaveFrecuenciaSuscripcionAsync(s);
                }
                return Ok(resource);
            }
            catch (UsuarioNoRegistradoException unre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("404", unre.Message);
                er.Errors.Add(e);
                return NotFound(er);
            }
            catch (TagNoRegistradaException tnre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("404", tnre.Message);
                er.Errors.Add(e);
                return NotFound(er);
            }
            catch (SuscripcionRegistradaException unre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("400", unre.Message);
                er.Errors.Add(e);
                return BadRequest(er);
            }
            catch (Exception ex)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("500", "Ha ocurrido un error inesperado.");
                er.Errors.Add(e);
                return StatusCode(500, er);
            }
        }


        /// <summary>
        /// Registro dispositivo
        /// </summary>
        /// <param name="resource"></param>
        /// <returns></returns>
        /// <response code="200">Dispositivo registrado</response>        
        ///<response code="400">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "400",
        ///      "detail": "Dispositivo ya registrada para el usuario."
        ///    }
        ///  ]
        ///}
        ///
        /// </response>
        /// <response code="401">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "401",
        ///      "detail": "Sin autorizacion."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// <response code="404">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "404",
        ///      "detail": "Usuario no registrado en el sistema."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// <response code="500">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "500",
        ///      "detail": "Ha ocurrido un error inesperado."
        ///    }
        ///  ]
        ///}
        /// </response>
        [HttpPost("{id}/dispositivo")]
        [Authorize]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> PostDispositivoAsync(long id, [FromBody] SaveDispositivoResource resource)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState.GetErrorMessages());

                var dispositivo = _mapper.Map<SaveDispositivoResource, Dispositivo>(resource);
                dispositivo.IdUsuarioTrabajo = id;
                dispositivo.Fecha = DateTime.Now;
                var result = await _usuarioService.SaveDispositivoAsync(dispositivo);

                if (!result.Success)
                    return BadRequest(result.Message);

                var dispositivoResource = _mapper.Map<Dispositivo, DispositivoResource>(result.Dispositivo);
                return Ok(dispositivoResource);
            }
            catch (UsuarioNoRegistradoException unre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("404", unre.Message);
                er.Errors.Add(e);
                return NotFound(er);
            }
            catch (DispositivoYaRegistradoException unre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("400", unre.Message);
                er.Errors.Add(e);
                return BadRequest(er);
            }
            catch (Exception ex)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("500", "Ha ocurrido un error inesperado.");
                er.Errors.Add(e);
                return StatusCode(500, er);
            }
        }

        /// <summary>
        /// Obtener dispositivos de Usuario
        /// </summary>
        /// <param name="resource"></param>
        /// <returns></returns>
        /// <response code="200">Listado de dispositivos</response>                
        /// <response code="401">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "401",
        ///      "detail": "Sin autorizacion."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// <response code="404">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "404",
        ///      "detail": "Usuario no registrado en el sistema."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// <response code="500">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "500",
        ///      "detail": "Ha ocurrido un error inesperado."
        ///    }
        ///  ]
        ///}
        /// </response>
        [HttpGet("{id}/dispositivo")]
        [Authorize]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> ListDispositivoAsync(long id)
        {
            try
            {
                var result = await _usuarioService.ListDspositivoAsync(id);
                var resources = _mapper.Map<IEnumerable<Dispositivo>, IEnumerable<DispositivoResource>>(result);
                return Ok(resources);
            }
            catch (UsuarioNoRegistradoException unre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("404", unre.Message);
                er.Errors.Add(e);
                return NotFound(er);
            }
            catch (Exception ex)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("500", "Ha ocurrido un error inesperado.");
                er.Errors.Add(e);
                return StatusCode(500, er);
            }
        }
        /// <summary>
        /// Eliminar dispositivo de Usuario
        /// </summary>
        /// <param name="resource"></param>        
        /// <returns></returns>
        /// <response code="200">Dispositivo eliminado</response>
        /// <response code="401">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "401",
        ///      "detail": "Sin autorizacion."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// /// <response code="404">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "404",
        ///      "detail": "Usuario no registrado en el sistema."
        ///    }
        ///    {
        ///      "status": "404",
        ///      "detail": "Dispositivo no registrado en el sistema."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// <response code="500">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "500",
        ///      "detail": "Ha ocurrido un error inesperado."
        ///    }
        ///  ]
        ///}
        /// </response>
        [HttpDelete("{id}/dispositivo/{iddispositivo}")]
        [Authorize]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> DeleteDispostivoAsync(long id, string iddispositivo)
        {
            try
            {
                var result = await _usuarioService.DeleteDispositivoAsync(id, iddispositivo);

                if (!result.Success)
                    return BadRequest(result.Message);

                var dispositivoResource = _mapper.Map<Dispositivo, DispositivoResource>(result.Dispositivo);
                return Ok(dispositivoResource);
            }
            catch (UsuarioNoRegistradoException unre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("404", unre.Message);
                er.Errors.Add(e);
                return NotFound(er);
            }
            catch (DispositivoNoRegistradoException unre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("404", unre.Message);
                er.Errors.Add(e);
                return NotFound(er);
            }
            catch (Exception ex)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("500", "Ha ocurrido un error inesperado.");
                er.Errors.Add(e);
                return StatusCode(500, er);
            }
        }
        
    }
}