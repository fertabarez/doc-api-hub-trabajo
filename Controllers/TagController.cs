﻿using AutoMapper;
using hubNotificacionesTrabajo.API.Domain.Exceptions.TagException;
using hubNotificacionesTrabajo.API.Domain.Models;
using hubNotificacionesTrabajo.API.Domain.Repositories;
using hubNotificacionesTrabajo.API.Domain.Services;
using hubNotificacionesTrabajo.API.Extensions;
using hubNotificacionesTrabajo.API.Resources;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Controllers
{
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class TagController: Controller
    {
        private readonly ITagService _tagService;
        private readonly ITagRepository _tagRepository;
        private readonly IMapper _mapper;

        public TagController(ITagService tagService, ITagRepository tagRepository, IMapper mapper)
        {
            _tagService = tagService;
            _tagRepository = tagRepository;
            _mapper = mapper;
        }
        /// <summary>
        /// Obtener Listado de Tags.
        /// </summary>        
        /// <param name="item"></param>
        /// <returns>A newly created TodoItem</returns>
        /// <response code="200">Listado de tags</response>
        /// <response code="401">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "401",
        ///      "detail": "Sin autorizacion."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// <response code="500">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "500",
        ///      "detail": "Ha ocurido un error."
        ///    }
        ///  ]
        ///}
        /// </response>
        [HttpGet]
        [Authorize]
        [ProducesResponseType(typeof(IEnumerable<TagResource>), 200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> GetAllAsync()
        {
            try
            {
                var tags = await _tagService.ListAsync();
                var resources = _mapper.Map<IEnumerable<Tag>, IEnumerable<TagResource>>(tags);
                return Ok(resources);
            }            
            catch (Exception ex)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("500", "Ha ocurrido un error inesperado.");
                er.Errors.Add(e);
                return StatusCode(500, er);
            }
            
        }
        /// <summary>
        /// Registrar Tag
        /// </summary>
        /// <param name="resource"></param>
        /// <returns></returns>
        /// <response code="200">Tag registrada</response>
        /// /// <response code="400">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "400",
        ///      "detail": "Tag ya registrada en el sistema."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// <response code="401">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "401",
        ///      "detail": "Sin autorizacion."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// <response code="500">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "500",
        ///      "detail": "Ha ocurido un error."
        ///    }
        ///  ]
        ///}
        /// </response>
        // registro tag
        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(TagResource),200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> PostAsync([FromBody] SaveTagResource resource)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState.GetErrorMessages());

                ////var existingTag = await _tagRepository.FindByIdAsync(resource.Descripcion);

                ////if (existingTag == null)
                ////    throw new UsuarioNoRegistradoException();

                var tag = _mapper.Map<SaveTagResource, Tag>(resource);
                tag.Fecha = DateTime.Now;
                var result = await _tagService.SaveAsync(tag);

                if (!result.Success)
                    return BadRequest(result.Message);

                var tagResource = _mapper.Map<Tag, TagResource>(result.Tag);
                return Ok(tagResource);
            }
            catch (TagRegistradaException tre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("400", tre.Message);
                er.Errors.Add(e);
                return BadRequest(er);
            }            
            catch (Exception ex)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("500", "Ha ocurrido un error inesperado.");
                er.Errors.Add(e);
                return StatusCode(500, er);
            }
        }

        /// <summary>
        /// Modificar Tag
        /// </summary>
        /// <param name="resource"></param>
        /// <returns></returns>
        /// <response code="200">Tag modificada</response>
        ///<response code="400">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "400",
        ///      "detail": "Tag ya registrada en el sistema."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// <response code="401">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "401",
        ///      "detail": "Sin autorizacion."
        ///    }
        ///  ]
        ///}
        /// </response>
        ///<response code="404">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "404",
        ///      "detail": "Tag no registrada en el sistema."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// <response code="500">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "500",
        ///      "detail": "Ha ocurido un error."
        ///    }
        ///  ]
        ///}
        /// </response>
        [HttpPut("{id}")]
        [Authorize]
        [ProducesResponseType(typeof(TagResource), 200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> PutAsync(long id, [FromBody] SaveTagResource resource)
        {
            
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState.GetErrorMessages());

                var tag = _mapper.Map<SaveTagResource, Tag>(resource);
                var result = await _tagService.UpdateAsync(id, tag);
                if (!result.Success)
                    return BadRequest(result.Message);

                var tagResource = _mapper.Map<Tag, TagResource>(result.Tag);
                return Ok(tagResource);
            }
            catch (TagRegistradaException tre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("400", tre.Message);
                er.Errors.Add(e);
                return BadRequest(er);
            }
            catch (TagNoRegistradaException tnre)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("404", tnre.Message);
                er.Errors.Add(e);
                return NotFound(er);
            }
            catch (Exception ex)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("500", "Ha ocurrido un error inesperado.");
                er.Errors.Add(e);
                return StatusCode(500, er);
            }

        }
        /// <summary>
        /// Eliminar Tag
        /// </summary>
        /// <param name="resource"></param>
        /// <returns></returns>
        /// <response code="200">Tag eliminada</response>
        /// <response code="401">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "401",
        ///      "detail": "Sin autorizacion."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// <response code="404">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "404",
        ///      "detail": "Tag no registrada en el sistema."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// <response code="500">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "500",
        ///      "detail": "Ha ocurido un error."
        ///    }
        ///  ]
        ///}
        /// </response>
        // baja de suscripcion de usuario        
        [HttpDelete("{id}")]
        [Authorize]
        [ProducesResponseType(typeof(TagResource), 200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> DeleteTagAsync(long id)
        {

            try
            {
                var result = await _tagService.DeleteTagAsync(id);

                if (!result.Success)
                    return BadRequest(result.Message);

                var tagResource = _mapper.Map<Tag, TagResource>(result.Tag);
                return Ok(tagResource);
            }
            catch (TagNoRegistradaException tnf )
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("404", tnf.Message);
                er.Errors.Add(e);
                return NotFound( er);
            }catch (Exception ex )
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("500", "Ha ocurrido un error inesperado.");
                er.Errors.Add(e);
                return StatusCode(500, er);
            }
            
        }
    }
}
