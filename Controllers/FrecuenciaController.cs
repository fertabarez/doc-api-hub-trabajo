﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using hubNotificacionesTrabajo.API.Domain.Models;
using hubNotificacionesTrabajo.API.Domain.Repositories;
using hubNotificacionesTrabajo.API.Domain.Services;
using hubNotificacionesTrabajo.API.Resources;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace hubNotificacionesTrabajo.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class FrecuenciaController : ControllerBase
    {
        private readonly IFrecuenciaService _frecuenciaService;        
        private readonly IMapper _mapper;

        public FrecuenciaController(IFrecuenciaService frecuenciaService, IMapper mapper)
        {
            _frecuenciaService = frecuenciaService;
            _mapper = mapper;
        }

        /// <summary>
        /// Obtener Listado de frecuencias.
        /// </summary>        
        /// <param name="item"></param>
        /// <returns>A newly created TodoItem</returns>
        /// <response code="200">Listado de frecuencias</response>
        /// <response code="401">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "401",
        ///      "detail": "Sin autorizacion."
        ///    }
        ///  ]
        ///}
        /// </response>
        /// <response code="500">        
        ///{
        ///  "errors": [
        ///    {
        ///      "status": "500",
        ///      "detail": "Ha ocurido un error."
        ///    }
        ///  ]
        ///}
        /// </response>
        [HttpGet]
        [Authorize]
        [ProducesResponseType(typeof(IEnumerable<FrecuenciaResource>), 200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> GetAllAsync()
        {
            try
            {
                var frecuencias = await _frecuenciaService.ListAsync();
                var resources = _mapper.Map<IEnumerable<Frecuencia>, IEnumerable<FrecuenciaResource>>(frecuencias);
                return Ok(resources);
            }
            catch (Exception ex)
            {
                ErrorResource er = new ErrorResource();
                Error e = new Error("500", "Ha ocurrido un error inesperado.");
                er.Errors.Add(e);
                return StatusCode(500, er);
            }
        }        

        // POST: api/Frecuencia
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Frecuencia/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
