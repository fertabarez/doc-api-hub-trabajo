﻿using hubNotificacionesTrabajo.API.Persistence.Context;

namespace hubNotificacionesTrabajo.API.Persistence.Repositories
{
    public abstract class BaseRepository
    {
        protected readonly AppDbContext _context;

        public BaseRepository(AppDbContext context)
        {
            _context = context;
        }
    }
}