﻿using hubNotificacionesTrabajo.API.Domain.Models;
using hubNotificacionesTrabajo.API.Domain.Repositories;
using hubNotificacionesTrabajo.API.Persistence.Context;
using hubNotificacionesTrabajo.API.Persistence.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Persistance.Repositories
{
    public class FrecuenciaRepository : BaseRepository, IFrecuenciaRepository
    {
        public FrecuenciaRepository(AppDbContext context) : base(context)
        {
            
        }
        public async Task<IEnumerable<Frecuencia>> ListAsync()
        {
            return await _context.Frecuencia.ToListAsync();
        }

        public async Task<Frecuencia> FindByIdAsync(long id)
        {
            return await _context.Frecuencia.FindAsync(id);
        }
    }
}
