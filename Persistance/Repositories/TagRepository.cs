﻿using hubNotificacionesTrabajo.API.Domain.Models;
using hubNotificacionesTrabajo.API.Domain.Repositories;
using hubNotificacionesTrabajo.API.Persistence.Context;
using hubNotificacionesTrabajo.API.Persistence.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Persistance.Repositories
{
    public class TagRepository : BaseRepository, ITagRepository
    {
        public TagRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Tag>> ListAsync()
        {
            return await _context.Tags.ToListAsync();
        }

        public async Task<Tag> FindByIdAsync(long id)
        {            
            return await _context.Tags.FindAsync(id);            
        }
        public Tag FindByDescAsync(string descripcion)
        {
            return _context.Tags.Where(s => s.Descripcion == descripcion).FirstOrDefault();            
        }
        public async Task AddAsync(Tag tag)
        {
            await _context.Tags.AddAsync(tag);
        }

        public void UpdateAsync(Tag tag)
        {
             _context.Tags.Update(tag);
        }
        public void Remove(Tag tag)
        {
             _context.Tags.Remove(tag);
        }

        public bool Find(IList<Suscripcion> suscripcions)
        {
            bool res  = true;
            Tag r = new Tag();
            foreach(Suscripcion s in suscripcions)
            {
                r = _context.Tags.Where(t => t.IdTag == s.IdTag).FirstOrDefault();
                if (r== null)
                {
                    res = false;
                    break;
                }
            }
            return res;           
        }
    }
}
