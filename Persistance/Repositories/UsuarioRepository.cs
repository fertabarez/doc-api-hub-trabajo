﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using hubNotificacionesTrabajo.API.Domain.Models;
using hubNotificacionesTrabajo.API.Domain.Repositories;
using hubNotificacionesTrabajo.API.Persistence.Context;
using System.Linq.Expressions;
using System.Linq;

namespace hubNotificacionesTrabajo.API.Persistence.Repositories
{
    public class UsuarioRepository : BaseRepository, IUsuarioRepository
    {
        public UsuarioRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Usuario>> ListAsync()
        {
            return await _context.Usuarios.ToListAsync();
        }

        public async Task<IList<Suscripcion>> ListSuscripcionAsync(long id)
        {
            return await _context.Suscripciones.Where(s => s.IdUsuarioTrabajo == id).ToListAsync();
        }
        public async Task<IList<FrecuenciaSuscripcion>> ListFrecuenciaSuscripcionAsync(long id)
        {
            return await _context.Frecuencias.Where(s => s.IdUsuarioTrabajo == id).ToListAsync();
        }

        public async Task AddAsync(Usuario usuario)
        {
            await _context.Usuarios.AddAsync(usuario);
        }  

        public async Task AddSuscripcionAsync(Suscripcion suscripcion)
        {
            await _context.Suscripciones.AddAsync(suscripcion);
        }

        public bool Find(string email, long idusuariotrabajo)
        {
            var find = _context.Usuarios.Where(u => u.Email == email || u.IdUsuarioTrabajo == idusuariotrabajo).FirstOrDefault();
            var res = find == null ? false : true;
            return res;
        }

        public async Task<Usuario> FindByIdAsync(long id)
        {
            return await _context.Usuarios.FindAsync(id);
        }
        public bool ExistIdUsuarioGallito(long id)
        {
            var find = _context.Usuarios.Where(u => u.IdUsuarioGallito == id).FirstOrDefault();
            var res = find == null ? false : true;
            return res;
        }
        public bool ExistEmailUsuario(string email)
        {
            var find = _context.Usuarios.Where(u => u.Email == email).FirstOrDefault();            
            var res = find == null ? false : true;
            return res;
        }

        public void Update(Usuario usuario)
        {
            _context.Usuarios.Update(usuario);
        }

        public void Remove(Usuario usuario)
        {
            _context.Usuarios.Remove(usuario);
        }

    }
}