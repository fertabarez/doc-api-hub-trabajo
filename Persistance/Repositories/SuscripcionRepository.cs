﻿using hubNotificacionesTrabajo.API.Domain.Models;
using hubNotificacionesTrabajo.API.Domain.Repositories;
using hubNotificacionesTrabajo.API.Persistence.Context;
using hubNotificacionesTrabajo.API.Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Persistance.Repositories
{
    public class SuscripcionRepository : BaseRepository, ISuscripcionRepository
    {
        public SuscripcionRepository(AppDbContext context) : base(context)
        {
        }

        public Suscripcion FindByIdAsync(long idUsuarioTrabajo, long idTag)
        {
            return _context.Suscripciones.Where(s => s.IdTag == idTag && s.IdUsuarioTrabajo == idUsuarioTrabajo).First();
        }

        public void Remove(Suscripcion suscripcion)
        {
            _context.Suscripciones.Remove(suscripcion);
        }

        public bool Find(long idTag, long idUsuario)
        {
            var find = _context.Suscripciones.Where(s => s.IdTag == idTag && s.IdUsuarioTrabajo == idUsuario).FirstOrDefault();
            var res = false;
            if ((find != null))
            {
                res = true;
            }
            return res;
        }
    }
}
