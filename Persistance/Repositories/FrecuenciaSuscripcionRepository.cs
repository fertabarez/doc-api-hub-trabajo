﻿using hubNotificacionesTrabajo.API.Domain.Models;
using hubNotificacionesTrabajo.API.Domain.Repositories;
using hubNotificacionesTrabajo.API.Persistence.Context;
using hubNotificacionesTrabajo.API.Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Persistance.Repositories
{
    public class FrecuenciaSuscripcionRepository: BaseRepository,IFrecuenciaSuscripcionRepository
    {
        public FrecuenciaSuscripcionRepository(AppDbContext context) : base(context)
        {

        }

        public async Task AddFrecuenciaSuscripcionAsync(FrecuenciaSuscripcion frecuenciaSuscripcion)
        {
            await _context.Frecuencias.AddAsync(frecuenciaSuscripcion);
        }
        public void Remove(FrecuenciaSuscripcion frecuenciaSuscripcion)
        {
            _context.Frecuencias.Remove(frecuenciaSuscripcion);
        }

        public bool Find(long idFrecuencia, long idUsuario)
        {
            var find = _context.Frecuencias.Where(s => s.IdFrecuencia == idFrecuencia && s.IdUsuarioTrabajo == idUsuario).FirstOrDefault();
            var res = false;
            if ((find != null))
            {
                res = true;
            }
            return res;
        }
    }
}
