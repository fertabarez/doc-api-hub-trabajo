﻿using hubNotificacionesTrabajo.API.Domain.Models;
using hubNotificacionesTrabajo.API.Domain.Repositories;
using hubNotificacionesTrabajo.API.Persistence.Context;
using hubNotificacionesTrabajo.API.Persistence.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Persistance.Repositories
{
    public class DispositivoRepository : BaseRepository, IDispositivoRepository
    {
        public DispositivoRepository(AppDbContext context) : base(context)
        {
        }

        public Dispositivo FindByIdAsync(long idUsuarioTrabajo, string idDispositivo)
        {
            return _context.Dispositivos.Where(s => s.IdDispositivo == idDispositivo && s.IdUsuarioTrabajo == idUsuarioTrabajo).FirstOrDefault();
        }

        public async Task AddDispositivoAsync(Dispositivo dispositivo)
        {
            await _context.Dispositivos.AddAsync(dispositivo);
        }

        public async Task<IList<Dispositivo>> ListDispositivoAsync(long id)
        {
            return await _context.Dispositivos.Where(s => s.IdUsuarioTrabajo == id).ToListAsync();
        }        

        public void Remove(Dispositivo dispositivo)
        {
            _context.Dispositivos.Remove(dispositivo);
        }

        public bool Find(IList<Dispositivo> dispositivos)
        {
            var res = false;
            foreach (Dispositivo dis in dispositivos)
            {
                if ((_context.Dispositivos.Where(d => d.IdDispositivo == dis.IdDispositivo).FirstOrDefault() != null))
                {
                    res = true;
                    break;
                }
            }            
            return res;
        }
    }
}
