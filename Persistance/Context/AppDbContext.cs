﻿using Microsoft.EntityFrameworkCore;
using System;
using hubNotificacionesTrabajo.API.Domain.Models;

namespace hubNotificacionesTrabajo.API.Persistence.Context
{
    public class AppDbContext : DbContext
    {
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Dispositivo> Dispositivos { get; set; }
        public DbSet<Suscripcion> Suscripciones { get; set; }
        public DbSet<FrecuenciaSuscripcion> Frecuencias { get; set; }
        public DbSet<Frecuencia> Frecuencia { get; set; }
        public DbSet<Tag> Tags { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options){
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // usuarios
            builder.Entity<Usuario>().ToTable("Usuarios");
            builder.Entity<Usuario>().HasKey(p => p.IdUsuarioTrabajo);
            builder.Entity<Usuario>().Property(p => p.IdUsuarioTrabajo).ValueGeneratedNever();
            builder.Entity<Usuario>().Property(p => p.IdUsuarioTrabajo).IsRequired().HasMaxLength(30);                       
            builder.Entity<Usuario>().Property(p => p.IdUsuarioGallito).IsRequired().HasMaxLength(30);
            builder.Entity<Usuario>().Property(p => p.Email).IsRequired().HasMaxLength(150);            
            builder.Entity<Usuario>().Property(p => p.Fecha).IsRequired().HasMaxLength(150);

            // dispositivo
            builder.Entity<Dispositivo>().ToTable("Dispositivos");
            builder.Entity<Dispositivo>().HasKey(p => new {p.IdUsuarioTrabajo, p.IdDispositivo });            
            builder.Entity<Dispositivo>().Property(p => p.IdDispositivo).IsRequired();
            builder.Entity<Dispositivo>().Property(p => p.VendorId).IsRequired();
            builder.Entity<Dispositivo>().Property(p => p.IdUsuarioTrabajo).IsRequired();
            builder.Entity<Dispositivo>().Property(p => p.Fecha).IsRequired().HasMaxLength(150);
            // claves foraneas
            builder.Entity<Dispositivo>().HasOne<Usuario>(s => s.Usuario).WithMany(g => g.Dispositivos).HasForeignKey(s => s.IdUsuarioTrabajo);

            // suscripcion
            builder.Entity<Suscripcion>().ToTable("Suscripciones");
            builder.Entity<Suscripcion>().HasKey(p => p.Id);
            builder.Entity<Suscripcion>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Suscripcion>().Property(p => p.IdUsuarioTrabajo).IsRequired().HasMaxLength(30);            
            builder.Entity<Suscripcion>().Property(p => p.Fecha).IsRequired().HasMaxLength(150);
            builder.Entity<Suscripcion>().Property(p => p.IdTag).IsRequired();
            // claves foraneas
            builder.Entity<Suscripcion>().HasOne<Usuario>(s => s.Usuario).WithMany(g => g.Suscripciones).HasForeignKey(s => s.IdUsuarioTrabajo);

            // frecuencia suscripcion
            builder.Entity<FrecuenciaSuscripcion>().ToTable("FrecuenciaSuscripcion");
            builder.Entity<FrecuenciaSuscripcion>().HasKey(p => p.Id);
            builder.Entity<FrecuenciaSuscripcion>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<FrecuenciaSuscripcion>().Property(p => p.IdUsuarioTrabajo).IsRequired().HasMaxLength(30);
            builder.Entity<FrecuenciaSuscripcion>().Property(p => p.Fecha).IsRequired().HasMaxLength(150);
            builder.Entity<FrecuenciaSuscripcion>().Property(p => p.IdFrecuencia).IsRequired();
            // claves foraneas
            builder.Entity<FrecuenciaSuscripcion>().HasOne<Usuario>(s => s.Usuario).WithMany(g => g.Frecuencias).HasForeignKey(s => s.IdUsuarioTrabajo);

            // frecuencia
            builder.Entity<Frecuencia>().ToTable("Frecuencia");
            builder.Entity<Frecuencia>().HasKey(p => p.IdFrecuencia);
            builder.Entity<Frecuencia>().Property(p => p.IdFrecuencia).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Frecuencia>().Property(p => p.Descripcion).IsRequired().HasMaxLength(200);            
            // claves foraneas
            builder.Entity<Frecuencia>().HasMany(c => c.Frecuencias).WithOne(e => e.Frecuencia).IsRequired();

            // tag
            builder.Entity<Tag>().ToTable("Tags");
            builder.Entity<Tag>().HasKey(p => p.IdTag);
            builder.Entity<Tag>().Property(p => p.IdTag).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Tag>().Property(p => p.Descripcion).IsRequired().HasMaxLength(200);
            builder.Entity<Tag>().Property(p => p.Fecha).IsRequired().HasMaxLength(150);
            // claves foraneas
            builder.Entity<Tag>().HasMany(c => c.Suscripcion).WithOne(e => e.Tag).IsRequired();
           
        }
    }
}

