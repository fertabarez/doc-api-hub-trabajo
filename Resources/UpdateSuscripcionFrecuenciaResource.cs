﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Resources
{
    public class UpdateSuscripcionFrecuenciaResource
    {
        public IList<TagResourceOnlyId> Suscripciones { get; set; } = new List<TagResourceOnlyId>();
        public IList<FrecuenciaSuscripcionResource> Frecuencias { get; set; } = new List<FrecuenciaSuscripcionResource>();
    }
}
