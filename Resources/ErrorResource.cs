﻿using hubNotificacionesTrabajo.API.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Resources
{
    public class ErrorResource
    {
        public IList<Error> Errors { get; set; } = new List<Error>();
    }
}
