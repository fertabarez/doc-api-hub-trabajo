﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Resources
{
    public class UpdateUsuarioResource
    {
        public long IdUsuarioGallito { get; set; }       
        public string Email { get; set; }
    }
}
