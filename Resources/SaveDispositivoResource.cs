﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Resources
{
    public class SaveDispositivoResource
    {
        public string IdDispositivo { get; set; }
        public string VendorId { get; set; }
    }
}
