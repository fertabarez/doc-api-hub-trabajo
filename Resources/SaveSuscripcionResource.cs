﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Resources
{
    public class SaveSuscripcionResource
    {        
        public IList<SuscripcionResource> Suscripciones { get; set; } = new List<SuscripcionResource>();
    }
}
