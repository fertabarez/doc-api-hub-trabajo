﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Resources
{
    public class SaveTagResource
    {
        [Required]
        [MaxLength(200)]
        public string Descripcion { get; set; }
        
    }
}
