﻿using System;

namespace hubNotificacionesTrabajo.API.Resources
{
    public class UsuarioResource
    {        
        public long IdUsuarioTrabajo { get; set; }
        public long IdUsuarioGallito { get; set; }
        public string Email { get; set; }
        public DateTime Fecha { get; set; }
    }
}
