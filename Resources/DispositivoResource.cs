﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Resources
{
    public class DispositivoResource
    {
        public long IdUsuarioTrabajo { get; set; }
        public string IdDispositivo { get; set; }
        public string VendorId { get; set; }
        public DateTime Fecha { get; set; }
    }
}
