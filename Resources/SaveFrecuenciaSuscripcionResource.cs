﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Resources
{
    public class SaveFrecuenciaSuscripcionResource
    {
        public IList<FrecuenciaSuscripcionResource> Frecuencias { get; set; } = new List<FrecuenciaSuscripcionResource>();
    }
}
