﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Resources
{
    public class SaveFrecuenciaResource
    {
        public long IdFrecuencia { get; set; }
        public string Descripcion { get; set; }
    }
}
