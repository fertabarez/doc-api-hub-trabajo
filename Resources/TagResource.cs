﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Resources
{
    public class TagResource
    {
        public long IdTag { get; set; }
        public string Descripcion { get; set; }
        public DateTime Fecha { get; set; }
    }
}
