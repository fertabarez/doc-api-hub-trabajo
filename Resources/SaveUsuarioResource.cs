﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using hubNotificacionesTrabajo.API.Domain.Models;

namespace hubNotificacionesTrabajo.API.Resources
{
    public class SaveUsuarioResource
    {
        [Required]        
        public long IdUsuarioTrabajo { get; set; }               
        public long IdUsuarioGallito { get; set; }
        [Required]        
        public string Email { get; set; }
        public IList<SaveDispositivoResource> Dispositivos { get; set; } = new List<SaveDispositivoResource>();
        public IList<SuscripcionResource> Suscripciones { get; set; } = new List<SuscripcionResource>();
        public IList<SaveFrecuenciaResource> Frecuencias { get; set; } = new List<SaveFrecuenciaResource>();

    }
}
