﻿using hubNotificacionesTrabajo.API.Domain.Exceptions.TagException;
using hubNotificacionesTrabajo.API.Domain.Models;
using hubNotificacionesTrabajo.API.Domain.Repositories;
using hubNotificacionesTrabajo.API.Domain.Services;
using hubNotificacionesTrabajo.API.Domain.Services.Communication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Services
{
    public class TagService: ITagService
    {
        private readonly ITagRepository _tagRepository;
        private readonly IUnitOfWork _unitOfWork;

        public TagService(ITagRepository tagRepository, IUnitOfWork unitOfWork)
        {
            _tagRepository = tagRepository;            
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Tag>> ListAsync()
        {
            return await _tagRepository.ListAsync();
        }

        public async Task<TagResponse> SaveAsync(Tag tag)
        {
            try
            {
                if (_tagRepository.FindByDescAsync(tag.Descripcion) != null)
                {
                    throw new TagRegistradaException();
                }
                else
                {
                    await _tagRepository.AddAsync(tag);
                    await _unitOfWork.CompleteAsync();
                }
                return new TagResponse(tag);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task<TagResponse> UpdateAsync(long id, Tag tag)
        {
            try
            {
                var existingTag =  await _tagRepository.FindByIdAsync(id);
                if (existingTag == null)
                {
                    throw new TagNoRegistradaException();
                }
                var existingDesc = _tagRepository.FindByDescAsync(tag.Descripcion);
                if (existingDesc != null && existingDesc.IdTag != id)
                {
                    throw new TagRegistradaException();
                }
                existingTag.Descripcion = tag.Descripcion;
                tag = existingTag;
                _tagRepository.UpdateAsync(existingTag);
                await _unitOfWork.CompleteAsync(); 
                return new TagResponse(tag);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task<TagResponse> DeleteTagAsync(long id)
        {
            try
            {
                var existingTag = await _tagRepository.FindByIdAsync(id);

                if (existingTag == null)
                {
                    throw new TagNoRegistradaException();
                }
                _tagRepository.Remove(existingTag);
                await _unitOfWork.CompleteAsync();
                return new TagResponse(existingTag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
