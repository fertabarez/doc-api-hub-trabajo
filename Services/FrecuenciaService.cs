﻿using hubNotificacionesTrabajo.API.Domain.Models;
using hubNotificacionesTrabajo.API.Domain.Repositories;
using hubNotificacionesTrabajo.API.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Services
{
    public class FrecuenciaService : IFrecuenciaService
    {
        private readonly IFrecuenciaRepository _frecuenciaRepository;
        private readonly IUnitOfWork _unitOfWork;

        public FrecuenciaService(IFrecuenciaRepository frecuenciaRepository, IUnitOfWork unitOfWork)
        {
            _frecuenciaRepository = frecuenciaRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Frecuencia>> ListAsync()
        {
            return await _frecuenciaRepository.ListAsync();
        }
    }
}
