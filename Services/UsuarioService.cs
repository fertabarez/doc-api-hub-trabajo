﻿using System.Collections.Generic;
using System.Threading.Tasks;
using hubNotificacionesTrabajo.API.Domain.Models;
using hubNotificacionesTrabajo.API.Domain.Services;
using hubNotificacionesTrabajo.API.Domain.Repositories;
using hubNotificacionesTrabajo.API.Domain.Services.Communication;
using System;
using hubNotificacionesTrabajo.API.Domain.Exceptions.UsuarioException;
using hubNotificacionesTrabajo.API.Domain.Exceptions.DispositivoException;
using hubNotificacionesTrabajo.API.Domain.Exceptions.SuscripcionException;
using hubNotificacionesTrabajo.API.Domain.Exceptions.TagException;
using hubNotificacionesTrabajo.API.Resources;
using hubNotificacionesTrabajo.API.Domain.Exceptions.FrecuenciaException;

namespace hubNotificacionesTrabajo.API.Services
{
    public class UsuarioService : IUsuarioService
    {
        private readonly IUsuarioRepository _usuarioRepository;
        private readonly IDispositivoRepository _dispositivoRepository;
        private readonly ISuscripcionRepository _suscripcionRepository;
        private readonly ITagRepository _tagRepository;
        private readonly IFrecuenciaRepository _frecuenciaRepository;
        private readonly IFrecuenciaSuscripcionRepository _frecuenciaSuscripcionRepository;
        private readonly IUnitOfWork _unitOfWork;

        public UsuarioService(IUsuarioRepository usuarioRepository, IDispositivoRepository dispositivoRepository, ISuscripcionRepository suscripcionRepository, ITagRepository tagRepository, IFrecuenciaRepository frecuenciaRepository, IFrecuenciaSuscripcionRepository frecuenciaSuscripcionRepository, IUnitOfWork unitOfWork)
        {
            _usuarioRepository = usuarioRepository;
            _dispositivoRepository = dispositivoRepository;
            _suscripcionRepository = suscripcionRepository;
            _tagRepository = tagRepository;
            _frecuenciaRepository = frecuenciaRepository;
            _frecuenciaSuscripcionRepository = frecuenciaSuscripcionRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Usuario>> ListUsuarioAsync()
        {
            return await _usuarioRepository.ListAsync();
        }

        public async Task<IEnumerable<Suscripcion>> ListSuscripcionAsync(long id)
        {
            try
            {
                var existingUsuario = await _usuarioRepository.FindByIdAsync(id);
                if (existingUsuario == null)
                    throw new UsuarioNoRegistradoException();


                IEnumerable<Suscripcion> suscripciones = await _usuarioRepository.ListSuscripcionAsync(id);

                foreach (Suscripcion ss in suscripciones)
                {
                    SuscripcionResource sr = new SuscripcionResource();
                    sr.IdTag = ss.IdTag;
                    Tag t = await _tagRepository.FindByIdAsync(ss.IdTag);
                    ss.Tag = t;
                }
                return suscripciones;
            }
            catch (Exception ex) 
            {
                throw ex;
            }
        }

        public async Task<IEnumerable<Dispositivo>> ListDspositivoAsync(long id)
        {
            return await _dispositivoRepository.ListDispositivoAsync(id);
        }

        public async Task<SaveUsuarioResponse> GetUsuarioById(long idUsuario)
        {

            try
            {
                var existingUsuario = await _usuarioRepository.FindByIdAsync(idUsuario);

                if (existingUsuario == null)
                {
                    throw new UsuarioNoRegistradoException();
                }
                else
                {
                    existingUsuario.Dispositivos = await _dispositivoRepository.ListDispositivoAsync(idUsuario);
                    existingUsuario.Suscripciones = await _usuarioRepository.ListSuscripcionAsync(idUsuario);
                    existingUsuario.Frecuencias = await _usuarioRepository.ListFrecuenciaSuscripcionAsync(idUsuario);
                    SaveUsuarioResource s = new SaveUsuarioResource();
                    s.IdUsuarioTrabajo = idUsuario;
                    s.IdUsuarioGallito = existingUsuario.IdUsuarioGallito;
                    s.Email = existingUsuario.Email;
                    IList<SuscripcionResource> suscripciones = new List<SuscripcionResource>();
                    IList<SaveDispositivoResource> dispositivos = new List<SaveDispositivoResource>();
                    IList<SaveFrecuenciaResource> frecuencias = new List<SaveFrecuenciaResource>();
                    foreach (Suscripcion ss in existingUsuario.Suscripciones)
                    {
                        SuscripcionResource sr = new SuscripcionResource();
                        sr.IdTag = ss.IdTag;
                        Tag t = await _tagRepository.FindByIdAsync(ss.IdTag);
                        sr.Descripcion = t.Descripcion;
                        suscripciones.Add(sr);
                    }
                    foreach (Dispositivo dd in existingUsuario.Dispositivos)
                    {
                        SaveDispositivoResource dr = new SaveDispositivoResource();
                        dr.IdDispositivo = dd.IdDispositivo;
                        dr.VendorId = dd.VendorId;
                        dispositivos.Add(dr);
                    }
                    foreach (FrecuenciaSuscripcion fs in existingUsuario.Frecuencias)
                    {
                        SaveFrecuenciaResource fr = new SaveFrecuenciaResource();
                        fr.IdFrecuencia = fs.IdFrecuencia;
                        Frecuencia f = await _frecuenciaRepository.FindByIdAsync(fs.IdFrecuencia);
                        fr.Descripcion = f.Descripcion;
                        frecuencias.Add(fr);
                    }
                    
                    s.Dispositivos = dispositivos;
                    s.Suscripciones = suscripciones;
                    s.Frecuencias = frecuencias;

                    return new SaveUsuarioResponse(s);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<UsuarioResponse> SaveUsuarioAsync(Usuario usuario)
        {
            try
            {
                if (_usuarioRepository.Find(usuario.Email, usuario.IdUsuarioTrabajo))
                {
                    throw new UsuarioRegistradoException();
                }                
                else if (!_tagRepository.Find(usuario.Suscripciones))
                {
                    throw new TagNoRegistradaException();
                }
                else
                {
                    await _usuarioRepository.AddAsync(usuario);
                    await _unitOfWork.CompleteAsync();
                }

                return new UsuarioResponse(usuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<UsuarioResponse> UpdateUsuarioAsync(long id, Usuario usuario)
        {
            try
            {
                var existingUsuario = await _usuarioRepository.FindByIdAsync(id);

                if (existingUsuario == null)
                    throw new UsuarioNoRegistradoException();

                if (!string.IsNullOrEmpty(usuario.Email))
                {
                    if (existingUsuario.Email != usuario.Email)
                    {
                        if (_usuarioRepository.ExistEmailUsuario(usuario.Email))
                        {
                            throw new UsuarioRegistradoException("Ya existe un usuario registrado con ese email.");
                        }
                    }

                }
                else
                {
                    throw new UsuarioException("El email de usuario no puede estar vacio.");
                }

                if (existingUsuario.IdUsuarioGallito > 0 && existingUsuario.IdUsuarioGallito != usuario.IdUsuarioGallito)
                {
                    if (_usuarioRepository.ExistIdUsuarioGallito(usuario.IdUsuarioGallito))
                    {
                        throw new UsuarioRegistradoException("Ya existe un usuario registrado en con ese id de usuario gallito en el sistema.");
                    }
                }

                _usuarioRepository.Update(existingUsuario);
                await _unitOfWork.CompleteAsync();

                return new UsuarioResponse(existingUsuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<UsuarioResponse> DeleteUsuarioAsync(long id)
        {
            try
            {
                var existingUsuario = await _usuarioRepository.FindByIdAsync(id);

                if (existingUsuario == null)
                    throw new UsuarioNoRegistradoException();

                _usuarioRepository.Remove(existingUsuario);
                await _unitOfWork.CompleteAsync();
                return new UsuarioResponse(existingUsuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<SuscripcionResponse> DeleteSuscripcionAsync(long idUsuarioTrabajo, long IdTag)
        {

            try
            {
                var existingUsuario = await _usuarioRepository.FindByIdAsync(idUsuarioTrabajo);

                if (existingUsuario == null)
                    throw new UsuarioNoRegistradoException();

                var existingSuscripcion = _suscripcionRepository.FindByIdAsync(idUsuarioTrabajo, IdTag);

                if (existingSuscripcion == null)
                    throw new SuscripcionNoRegistradaException();


                _suscripcionRepository.Remove(existingSuscripcion);
                await _unitOfWork.CompleteAsync();

                return new SuscripcionResponse(existingSuscripcion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<SuscripcionResponse> DeleteAllSuscripcionAsync(long idUsuarioTrabajo)
        {

            try
            {
                var existingUsuario = await _usuarioRepository.FindByIdAsync(idUsuarioTrabajo);

                if (existingUsuario == null)
                    throw new UsuarioNoRegistradoException();

                var existingSuscripcion = await _usuarioRepository.ListSuscripcionAsync(idUsuarioTrabajo);

                foreach (Suscripcion s in existingSuscripcion)
                {
                    _suscripcionRepository.Remove(s);
                    await _unitOfWork.CompleteAsync();
                }

                return new SuscripcionResponse(true, "true", new Suscripcion());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<FrecuenciaSuscripcionResponse> DeleteAllFrecuenciasAsync(long idUsuarioTrabajo)
        {

            try
            {
                var existingUsuario = await _usuarioRepository.FindByIdAsync(idUsuarioTrabajo);

                if (existingUsuario == null)
                    throw new UsuarioNoRegistradoException();

                var existingFrecuenciaSuscripcion = await _usuarioRepository.ListFrecuenciaSuscripcionAsync(idUsuarioTrabajo);

                foreach (FrecuenciaSuscripcion f in existingFrecuenciaSuscripcion)
                {
                    _frecuenciaSuscripcionRepository.Remove(f);
                    await _unitOfWork.CompleteAsync();
                }

                return new FrecuenciaSuscripcionResponse(true, "true", new FrecuenciaSuscripcion());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DispositivoResponse> DeleteDispositivoAsync(long idUsuarioTrabajo, string IdDispositivo)
        {


            try
            {
                var existingUsuario = await _usuarioRepository.FindByIdAsync(idUsuarioTrabajo);

                if (existingUsuario == null)
                    throw new UsuarioNoRegistradoException();

                var existingDispositivo = _dispositivoRepository.FindByIdAsync(idUsuarioTrabajo, IdDispositivo);

                if (existingDispositivo == null)
                    throw new DispositivoNoRegistradoException();

                _dispositivoRepository.Remove(existingDispositivo);
                await _unitOfWork.CompleteAsync();

                return new DispositivoResponse(existingDispositivo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<SuscripcionResponse> SaveSuscripcionAsync(Suscripcion suscripcion)
        {
            try
            {
                var existingUsuario = await _usuarioRepository.FindByIdAsync(suscripcion.IdUsuarioTrabajo);

                if (existingUsuario == null)
                    throw new UsuarioNoRegistradoException();

                var existingTag = await _tagRepository.FindByIdAsync(suscripcion.IdTag);

                if (existingTag == null)
                    throw new TagNoRegistradaException();

                if (!_suscripcionRepository.Find(suscripcion.IdTag, suscripcion.IdUsuarioTrabajo))
                {
                    await _usuarioRepository.AddSuscripcionAsync(suscripcion);
                    await _unitOfWork.CompleteAsync();
                }                

                return new SuscripcionResponse(suscripcion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<FrecuenciaSuscripcionResponse> SaveFrecuenciaSuscripcionAsync(FrecuenciaSuscripcion frecuenciaSuscripcion)
        {
            try
            {
                var existingUsuario = await _usuarioRepository.FindByIdAsync(frecuenciaSuscripcion.IdUsuarioTrabajo);

                if (existingUsuario == null)
                    throw new UsuarioNoRegistradoException();

                var existingFrecuencia = await _frecuenciaRepository.FindByIdAsync(frecuenciaSuscripcion.IdFrecuencia);

                if (existingFrecuencia == null)
                    throw new FrecuenciaNoRegistradaException();

                if (!_frecuenciaSuscripcionRepository.Find(frecuenciaSuscripcion.IdFrecuencia, frecuenciaSuscripcion.IdUsuarioTrabajo))
                {
                    await _frecuenciaSuscripcionRepository.AddFrecuenciaSuscripcionAsync(frecuenciaSuscripcion);
                    await _unitOfWork.CompleteAsync();
                }
                return new FrecuenciaSuscripcionResponse(frecuenciaSuscripcion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<DispositivoResponse> SaveDispositivoAsync(Dispositivo dispositivo)
        {
            try
            {
                var existingUsuario = await _usuarioRepository.FindByIdAsync(dispositivo.IdUsuarioTrabajo);

                if (existingUsuario == null)
                    throw new UsuarioNoRegistradoException();

                if (_dispositivoRepository.FindByIdAsync(dispositivo.IdUsuarioTrabajo, dispositivo.IdDispositivo) == null)
                {
                    await _dispositivoRepository.AddDispositivoAsync(dispositivo);
                    await _unitOfWork.CompleteAsync();
                }

                return new DispositivoResponse(dispositivo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ValidarUsuario(Usuario usuario)
        {

        }

    }
}