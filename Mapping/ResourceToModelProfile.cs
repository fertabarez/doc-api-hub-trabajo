﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hubNotificacionesTrabajo.API.Resources;
using hubNotificacionesTrabajo.API.Domain.Models;
using AutoMapper;

namespace hubNotificacionesTrabajo.API.Mapping
{
    public class ResourceToModelProfile : Profile
    {
        public ResourceToModelProfile()
        {
            CreateMap<SaveUsuarioResource, Usuario>();
            CreateMap<SaveSuscripcionResource, Suscripcion>();
            CreateMap<SuscripcionResource, Suscripcion>();
            CreateMap<TagResource, Tag>();
            CreateMap<SaveTagResource, Tag>();
            CreateMap<SaveDispositivoResource, Dispositivo>();
            CreateMap<SaveDispositivoResource, IList<Dispositivo>>();
            CreateMap<SaveFrecuenciaResource, FrecuenciaSuscripcion>();
            CreateMap<SaveFrecuenciaResource, IList<FrecuenciaSuscripcion>>();
            CreateMap<FrecuenciaSuscripcionResource, FrecuenciaSuscripcion>();
            CreateMap<TagResourceOnlyId, Suscripcion>();
        }
    }
}
