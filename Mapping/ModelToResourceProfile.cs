﻿using AutoMapper;
using hubNotificacionesTrabajo.API.Domain.Models;
using hubNotificacionesTrabajo.API.Resources;

namespace hubNotificacionesTrabajo.API.Mapping
{
    public class ModelToResourceProfile : Profile
    {
        public ModelToResourceProfile()
        {
            CreateMap<Usuario, UsuarioResource>();
            CreateMap<Usuario, SaveUsuarioResource>()
                .ForMember(u => u.Dispositivos, s => s.MapFrom(sw => sw.Dispositivos))
                .ForMember(u => u.Suscripciones, s => s.MapFrom(sw => sw.Suscripciones));
                
            CreateMap<Suscripcion, SuscripcionResource>()
                .ForMember(s => s.Descripcion, s => s.MapFrom(sw => sw.Tag.Descripcion));

            CreateMap<Suscripcion, SaveSuscripcionResource>();
            CreateMap<Tag, TagResource>();
            CreateMap<Dispositivo, DispositivoResource>();
            CreateMap<Frecuencia, FrecuenciaResource>();
        }
    }
}
