﻿using AutoMapper;
using hubNotificacionesTrabajo.API.Domain.Models;
using hubNotificacionesTrabajo.API.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hubNotificacionesTrabajo.API.Mapping
{
    public class UpdateUsuarioResourceToModelProfilecs : Profile
    {
        public UpdateUsuarioResourceToModelProfilecs()
        {
            CreateMap<UpdateUsuarioResource, Usuario>();
        }
    }
}
