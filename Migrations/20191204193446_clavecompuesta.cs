﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace hubNotificacionesTrabajo.API.Migrations
{
    public partial class clavecompuesta : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Dispositivos",
                table: "Dispositivos");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_Dispositivos_IdUsuarioTrabajo",
                table: "Dispositivos");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Dispositivos",
                table: "Dispositivos",
                columns: new[] { "IdUsuarioTrabajo", "IdDispositivo" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Dispositivos",
                table: "Dispositivos");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Dispositivos",
                table: "Dispositivos",
                column: "IdDispositivo");

            migrationBuilder.AddUniqueConstraint(
                name: "AK_Dispositivos_IdUsuarioTrabajo",
                table: "Dispositivos",
                column: "IdUsuarioTrabajo");
        }
    }
}
